$(function() {

	var margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 60
	},
	width = 700 - margin.left - margin.right,
	height = 350 - margin.top - margin.bottom;
	pad = -70;

	var x = d3.scale.ordinal()
	    .rangeRoundBands([0, width], .1);

	var y = d3.scale.linear()
	    .rangeRound([height, 0]);

	var xAxis = d3.svg.axis()
	    .scale(x)
	    .orient("bottom");

	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left")
	    .tickFormat(d3.format(".2s"));

	var color = d3.scale.ordinal()
	    .range(["#A0CFEC", "#e8adaa"]);



	var tip = d3.tip()
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function(d) {
			return "<strong>"+d.name+"</strong> <span style='color:red'>" + Math.round(d.y1-d.y0) + "</span>";
		  })

	var svg = d3.select("div#visual")
		.append("svg")
			.attr("preserveAspectRatio","xMinYMin meet")
			.attr("viewBox","0 0 800 500")
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	svg.call(tip);

	d3.csv("v3.csv", function (error, data) {
		
		data.forEach(function (d) {
		    d.Male = +d.Male;
		    d.Female = +d.Female;
		});

		color.domain(d3.keys(data[0]).filter(function (key) {
				    return key !== "Age" && key!== "No";
		}));

		data.forEach(function (d) {
		    var y0 = 0;
		    d.sex = color.domain().map(function (name) {
				return {
		            name: name,
		            y0: y0,
		            y1: y0 += +d[name]	            
	        	};
	    	});

	    	d.total = d.sex[d.sex.length - 1].y1;

	 	});
//console.log(data);
	 	data.sort(function (a, b) {
    		return a.No - b.No;
		});

	 	x.domain(data.map(function (d) {
		    return d.Age;
		}));

		y.domain([0, d3.max(data, function (d) {
		    return d.total;
		})]);

		svg.append("g")
	    .attr("class", "x axis")
	    .attr("transform", "translate(0," + height + ")")
	    .call(xAxis).selectAll("text")
	    .attr("y", 0)
	    .attr("x", 9)
	    .attr("dy", ".35em")
	    .attr("transform", "rotate(90)")
	    .style("text-anchor", "start");

	    svg.append("g")
	    .attr("class", "y axis")
	    .call(yAxis);


	    var age = svg.selectAll(".age")
		    .data(data)
		    .enter().append("g")
		    .attr("class", "g")
		    .attr("transform", function (d) { return "translate(" + x(d.Age) + ",0)";});

		age.selectAll("rect")
		    .data(function (d) {return d.sex;})
		    .enter().append("rect")
		    .attr("width", x.rangeBand())
		    .attr("y", function (d) {return y(d.y1);})
		    .attr("height", function (d) {return y(d.y0) - y(d.y1);})
		    .style("fill", function (d) {return color(d.name);})
		    .on('mouseover', tip.show)
      		.on('mouseout', tip.hide);


		svg.append("text")
            .attr("text-anchor", "middle")  
            .attr("transform", "translate("+ (pad/2) +","+(height/2)+")rotate(-90)")  
            .text("Count of people died");

        svg.append("text")
            .attr("text-anchor", "middle")  
            .attr("transform", "translate("+ (width/2) +","+(height-(pad))+")")  
            .text("Age Group");


		var legend = svg.selectAll(".legend")
		    .data(color.domain().slice().reverse())
		    .enter().append("g")
		    .attr("class", "legend")
		    .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")";});

		legend.append("rect")
		    .attr("x", width + 55)
		    .attr("width", 18)
		    .attr("height", 18)
		    .style("fill", color);

		legend.append("text")
		    .attr("x", width + 50)
		    .attr("y", 9)
		    .attr("dy", ".35em")
		    .style("text-anchor", "end")
		    .text(function (d) {return d;});




	});

})